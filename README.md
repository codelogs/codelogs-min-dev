# codelogs-min-dev
Imagen de desarrollo ligera, basada en alpine

### INFO

- Claves SSH para ahcer deploy en maven repo codelogs
- .m2/settings.xml con los perfiles maven del proyecto
- scripts utiles
- maven 3.3.9
- Open Jdk 1.8
- Node
- Bower
- Make
- g++
- python

